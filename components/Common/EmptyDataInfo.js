import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
Icon.loadFont();

const EmptyDataInfo = (props) => {
    return (
        <View style={styles.container}>
            {(props.loading) ?
            ( <Text style={styles.message}>Yükleniyor</Text>) : 
            (
            <React.Fragment>
            <Icon
                name="ios-information-circle-outline"
                color="#000"
                size={48}
                />
                <Text style={styles.message}>İlgili kriterlere uygun kayıt bulunmamakta</Text></React.Fragment>)
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 50
    },
    message: {
        fontSize: 18,
        marginTop: 20
    }
})

export default EmptyDataInfo
