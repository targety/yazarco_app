import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'
Icon.loadFont();

const HomeWithNoData = (props) => {
    return (
        <React.Fragment>
            <View style={{alignItems: 'center', marginTop: 50}}>
                <Icon
                name="ios-information-circle-outline"
                color="#1a202c"
                size={48}
                />
            </View>
            <Text style={styles.info}>Takip ettiğiniz yazarların, seçili tarihe göre köşe yazısı bulunmamaktadır.</Text>
            <View style={{padding: 20}}>
                <TouchableOpacity activeOpacity={0.8} style={styles.button} onPress={() => props.navigation.navigate('Explore')}>
                    <Text style={styles.buttonLabel}>Keşfet</Text>
                </TouchableOpacity>
            </View>
        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    info: {
        paddingTop: 15,
        paddingHorizontal: 20,
        fontSize: 16,
        lineHeight: 24,
        color: '#1a202c',
    },
    button: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#4a5568',
        padding: 10,
        marginTop: 20
    },
    buttonLabel: {
        color: '#fff', 
        fontSize: 18, 
        fontWeight: 'bold'
    },
})

export default withNavigation(HomeWithNoData)
