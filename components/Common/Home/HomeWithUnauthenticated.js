import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'
Icon.loadFont();

const HomeWithUnauthenticated = (props) => {
    return (
        <React.Fragment>
            <View style={{alignItems: 'center'}}>
                <Icon
                name="ios-information-circle-outline"
                color="#1a202c"
                size={48}
                />
            </View>
            <Text style={styles.info}>Üye girişi yaptığınızda, Anasayfanız takip ettiğiniz yazarların köşe yazılarına göre listelenecektir.</Text>
            <TouchableOpacity activeOpacity={0.5} onPress={() => props.navigation.navigate('Explore')}>
                <Text style={styles.info}>Üye girişi yapmadan, günün köşe yazılarını okumak için Keşfet sayfasını ziyaret edebilirsiniz.</Text>
            </TouchableOpacity>
            <View style={{padding: 20}}>
                <TouchableOpacity activeOpacity={0.8} style={styles.button} onPress={() => props.navigation.navigate('Login')}>
                    <Text style={styles.buttonLabel}>Üye Girişi</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.button} onPress={() => props.navigation.navigate('Register')}>
                    <Text style={styles.buttonLabel}>Kayıt Ol</Text>
                </TouchableOpacity>
            </View>
        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    info: {
        paddingTop: 15,
        paddingHorizontal: 20,
        fontSize: 16,
        lineHeight: 24,
        color: '#1a202c',
    },
    button: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#4a5568',
        padding: 10,
        marginTop: 20
    },
    buttonLabel: {
        color: '#fff', 
        fontSize: 18, 
        fontWeight: 'bold'
    },
})

export default withNavigation(HomeWithUnauthenticated)
