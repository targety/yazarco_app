import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { AUTHOR_IMAGE_ROOT_URL } from '../../store/constants'

const AuthorInfoCard = (props) => {
    let followings = (props.isFollowing ?  
        (<View style={styles.unfollowButton}><Text style={styles.unfollowButtonLabel}>Takip Ediliyor</Text></View>) : 
        (<View style={styles.followButton}><Text style={styles.followButtonLabel}>Takip Et</Text></View>)
        )

    return (
        <View style={{flexDirection: 'column', padding: 10,}}>
            <View style={{ paddingBottom: 10, alignItems: 'center', position: 'relative'}}>
                <Image 
                    source={{uri: AUTHOR_IMAGE_ROOT_URL + props.image}}
                    style={styles.authorImage}/>
                <View style={{position: 'absolute', right: 0, top: 20}}>
                    <TouchableOpacity
                    hitSlop={{top: 20, bottom: 20, left: 10, right: 10}}
                    onPress={(id, token) => (props.isFollowing ? props.unfollowAuthor(props.id, props.token) : props.followAuthor(props.id, props.token)) }>
                        <React.Fragment>
                            {followings}
                        </React.Fragment>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.authorInfoContainer}>
                <View style={styles.authorInfo}>
                    <Text style={styles.authorInfoTitle}>1210</Text>
                    <Text style={styles.authorInfoDescription}>Köşe Yazısı</Text>
                </View>
                <View style={styles.authorInfo}>
                    <Text style={styles.authorInfoTitle}>11b</Text>
                    <Text style={styles.authorInfoDescription}>Takipçi</Text>
                </View>
                <View style={styles.authorInfo}>
                    <Text style={styles.authorInfoTitle}>350b</Text>
                    <Text style={styles.authorInfoDescription}>Okunma</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    authorImage: {
        height: 70,
        width: 70,
        padding: 5,
        borderWidth:2,
        borderColor: '#edf2f7',
        borderRadius: 35,
        backgroundColor: '#ffffff'
    },
    authorInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 50
    },
    authorInfo: {
        alignItems: 'center'
    },
    authorInfoTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1a202c'
    },
    authorInfoDescription: {
        fontSize: 11,
        fontWeight: '700',
        color: '#8c8c8c'
    },
    followButton: {
        width: 90,
        alignItems: 'center',
        borderColor: '#DC3030',
        borderWidth: 1,
        borderRadius: 20,
        paddingVertical: 5,
    },
    unfollowButton: {
        width: 90,
        alignItems: 'center',
        borderColor: '#DC3030',
        backgroundColor: '#DC3030',
        borderWidth: 1,
        borderRadius: 20,
        paddingVertical: 5,
    },
    followButtonLabel: {
        color: '#DC3030',
        fontSize: 10,
        fontWeight: 'bold'
    },
    unfollowButtonLabel: {
        color: 'white',
        fontSize: 10,
        fontWeight: 'bold'
    },
})

export default AuthorInfoCard
