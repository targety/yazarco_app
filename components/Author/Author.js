import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import {AUTHOR_IMAGE_ROOT_URL, PUBLISHER_IMAGE_ROOT_URL} from '../../store/constants'

const Author = (props) => {
    let followings = (props.isFollowing ?  
        (<View style={styles.unfollowButton}><Text style={styles.unfollowButtonLabel}>Takip Ediliyor</Text></View>) : 
        (<View style={styles.followButton}><Text style={styles.followButtonLabel}>Takip Et</Text></View>)
        )

    return (
        <View style={styles.authorContainer}>
            <View style={styles.authorLeftContainer}>
            <Image 
                defaultSource={require('../../images/blank.png')}
                source={{uri: AUTHOR_IMAGE_ROOT_URL + props.data.image}}
                style={styles.authorImage}  />
            </View>
            <View style={{flex:1}}>
                <Text style={styles.authorName}>{props.data.name}</Text>
                <View style={{flexDirection: 'row'}}>
                {
                props.data.publishers.map(publisher => (<Image key={(Math.floor(Math.random() * 100000))} source={{uri: PUBLISHER_IMAGE_ROOT_URL + publisher.image}} style={styles.publisherImage}  />))
                }
                </View>
            </View>
            <View style={{justifyContent: 'center'}}>
                <TouchableOpacity
                hitSlop={{top: 20, bottom: 20, left: 10, right: 10}}
                onPress={(id, token) => (props.isFollowing ? props.unfollowAuthor(props.data.id, props.token) : props.followAuthor(props.data.id, props.token)) }>
                    <React.Fragment>
                        {followings}
                    </React.Fragment>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    authorContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
    },
    authorLeftContainer: {
        width: 60,
        alignItems: 'center',
        marginRight: 10,
        backgroundColor: '#fff'
    },
    authorImage: {
        height: 60,
        width: 60,
        padding: 5,
        borderWidth:2,
        borderColor: '#edf2f7',
        borderRadius: 30,
        backgroundColor: '#ffffff'
    },
    publisherImage: {
        marginTop: 10,
        width: 45,
        height: 15,
        resizeMode: 'contain',
        marginRight: 10
    },
    followButton: {
        width: 90,
        alignItems: 'center',
        borderColor: '#DC3030',
        borderWidth: 1,
        borderRadius: 20,
        paddingVertical: 5,
    },
    unfollowButton: {
        width: 90,
        alignItems: 'center',
        borderColor: '#DC3030',
        backgroundColor: '#DC3030',
        borderWidth: 1,
        borderRadius: 20,
        paddingVertical: 5,
    },
    followButtonLabel: {
        color: '#DC3030',
        fontSize: 10,
        fontWeight: 'bold'
    },
    unfollowButtonLabel: {
        color: 'white',
        fontSize: 10,
        fontWeight: 'bold'
    },
    authorName: {
        color: '#1a202c',
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 10
    }
})

export default Author