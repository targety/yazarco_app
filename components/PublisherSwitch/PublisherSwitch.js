import React from 'react'
import { View, Text, Switch, StyleSheet } from 'react-native'

const PublisherSwitch = (props) => {
    return (
        <View style={styles.publisherGroup}>
            <Text style={styles.publisher}>{props.publisher}</Text>
            <Switch 
            onValueChange={props.toggle}
            value={props.is_visible} />
        </View>
    )
}

const styles = StyleSheet.create({
    publisherGroup: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
    },
    publisher: {
        fontSize: 18,
        flex: 1
    },
})

export default PublisherSwitch
