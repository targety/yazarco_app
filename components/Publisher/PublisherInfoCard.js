import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { PUBLISHER_IMAGE_ROOT_URL } from '../../store/constants'

const PublisherInfoCard = (props) => {
    return (
        <View style={{flexDirection: 'column', padding: 10,}}>
            <View style={{ paddingBottom: 10, alignItems: 'center'}}>
                <Image 
                    source={{uri: PUBLISHER_IMAGE_ROOT_URL + props.image}}
                    style={styles.publisherImage}/>
            </View>
            <View style={styles.publisherInfoContainer}>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>1210</Text>
                    <Text style={styles.publisherInfoDescription}>Köşe Yazarı</Text>
                </View>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>11b</Text>
                    <Text style={styles.publisherInfoDescription}>Köşe Yazısı</Text>
                </View>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>350b</Text>
                    <Text style={styles.publisherInfoDescription}>Okunma</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    publisherImage: {
        width: 120,
        height: 40,
        resizeMode: 'contain'
    },
    publisherInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 50
    },
    publisherInfo: {
        alignItems: 'center'
    },
    publisherInfoTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1a202c'
    },
    publisherInfoDescription: {
        fontSize: 11,
        fontWeight: '700',
        color: '#8c8c8c'
    },
})

export default PublisherInfoCard
