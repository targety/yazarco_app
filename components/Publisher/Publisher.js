import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import {PUBLISHER_IMAGE_ROOT_URL} from '../../store/constants';


const Publisher = (props) => {
    return (
        <View style={styles.publisherContainer}>
            <View>
                <Image 
                    source={{uri: PUBLISHER_IMAGE_ROOT_URL + props.data.image}}
                    style={styles.publisherImage}
                    />
            </View>
            <View style={styles.publisherInfoContainer}>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>1210</Text>
                    <Text style={styles.publisherInfoDescription}>Köşe Yazarı</Text>
                </View>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>11b</Text>
                    <Text style={styles.publisherInfoDescription}>Köşe Yazısı</Text>
                </View>
                <View style={styles.publisherInfo}>
                    <Text style={styles.publisherInfoTitle}>350b</Text>
                    <Text style={styles.publisherInfoDescription}>Okunma</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    publisherContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        padding: 20,
    },
    publisherInfoContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between'
    },
    publisherInfo: {
        alignItems: 'center',
        flex: 1
    },
    publisherInfoTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1a202c'
    },
    publisherInfoDescription: {
        fontSize: 11,
        fontWeight: '700',
        color: '#8c8c8c'
    },
    publisherImage: {
        width: 90,
        height: 30,
        resizeMode: 'contain'
    },
})

export default Publisher
