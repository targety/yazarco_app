import React from 'react'
import { View, StyleSheet } from 'react-native'
import ArticleHeader from './ArticleHeader/ArticleHeader'
import ArticleContent from './ArticleContent/ArticleContent'
import ArticleFooter from './ArticleFooter/ArticleFooter'

const ArticleRight = (props) => {
    return (
        <View style={styles.articleRightContainer}>
            <ArticleHeader data={props.data} />
            <ArticleContent article={props.data} />
            <ArticleFooter isFavorite={props.isFavorite} isBookmark={props.isBookmark} token={props.token} removeFavorite={props.removeFavorite} addFavorite={props.addFavorite} removeBookmark={props.removeBookmark} addBookmark={props.addBookmark} shareUrl={props.shareUrl} article={props.data} />
        </View>
    )
}

const styles = StyleSheet.create({
    articleRightContainer: {
        marginLeft: 10,
        flex:1,
    },  
})

export default ArticleRight
