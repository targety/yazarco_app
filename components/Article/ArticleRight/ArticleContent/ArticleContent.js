import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const ArticleContent = (props) => {
    return (
        <View>
            <Text style={styles.articleTitle}>{props.article.title}</Text>
            <Text style={styles.articleDescription}>{props.article.description}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    articleTitle: {
        color: '#1a202c',
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom: 5
    },
    articleDescription: {
        color: '#1a202c',
        fontSize: 14,
        fontWeight: 'normal',
        lineHeight: 20
    },
})

export default ArticleContent
