import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { withNavigation } from 'react-navigation';
Icon.loadFont();

const ArticleFooter = (props) => {

    let favorited = (props.isFavorite ?  (
        <Icon
            name='heart' 
            style={styles.articleFavorited} size={20} />
        ) : (
        <Icon
            name='heart-outline' 
            style={styles.articleFooterIcon} size={20} />
        ))
    
    let bookmarked = (props.isBookmark ?  (
        <Icon
            name='bookmark' 
            style={styles.articleBookmarked} size={20} />
        ) : (
        <Icon
            name='bookmark-outline' 
            style={styles.articleFooterIcon} size={20} />
        ))

    return (
        <View style={styles.articleFooter}>
            <View style={styles.articleFooterSection}><Icon name='book-open-outline' style={styles.articleFooterIcon} size={20} /><Text style={styles.articleFooterInfo}>{props.article.total_reading}</Text></View>
            <View style={styles.articleFooterSection}>
                <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} style={{flexDirection: 'row', alignItems: 'center',}} 
                onPress={(id, token) => (props.isFavorite ? props.removeFavorite(props.article.id, props.token) : props.addFavorite(props.article.id, props.token)) }>
                    <React.Fragment>
                        {favorited}
                    </React.Fragment>
                </TouchableOpacity>
            </View>
            <View style={styles.articleFooterSection}>
                <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} style={{flexDirection: 'row', alignItems: 'center',}} 
                onPress={(id, token) => (props.isBookmark ? props.removeBookmark(props.article.id, props.token) : props.addBookmark(props.article.id, props.token)) }>
                    <React.Fragment>
                        {bookmarked}
                    </React.Fragment>
                </TouchableOpacity>
            </View>
            <View style={styles.articleFooterSection}>
                <TouchableOpacity hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} style={{flexDirection: 'row', alignItems: 'center',}} 
                onPress={(article_url) => (props.shareUrl(props.article.article_url)) }>
                    <Icon name='share-variant' style={styles.articleFooterIcon} size={20} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    articleFooter: {
        flexDirection: 'row',
    },
    articleFooterSection: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },
    articleFooterIcon: {
        color: '#8A8A8F',
        marginRight: 5,
    },
    articleFavorited:{
        color: '#f00',
        marginRight: 5
    },
    articleBookmarked:{
        color: '#8A8A8F',
        marginRight: 5
    },
    articleFooterInfo: {
        color: '#8A8A8F',
        fontSize: 12
    }
})

export default withNavigation(ArticleFooter)
