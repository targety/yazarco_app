import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
Icon.loadFont();

const ArticleHeader = (props) => {
    return (
        <View style={styles.articleHeader}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => props.navigation.navigate('AuthorDetail', {id: props.data.author.id, name: props.data.author.name, image: props.data.author.image})}>
                <Text style={styles.authorName}>{props.data.author.name}</Text>
            </TouchableOpacity>
            <Text style={styles.articleDate}>{moment(props.data.publish_date, 'YYYY-MM-DD').format("DD.MM.YYYY")}</Text>
            {/* <Icon name='chevron-down' style={styles.articleChevron} size={24} /> */}
        </View>
    )
}

const styles = StyleSheet.create({
    articleHeader: {
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    articleChevron: {
        color: '#8c8c8c'
    },
    authorName: {
        color: '#8c8c8c',
        fontSize: 12,
        fontWeight: 'bold',
    },
    articleDate: {
        color: '#8c8c8c',
        fontSize: 11,
        fontWeight: 'normal',
    },
});

export default withNavigation(ArticleHeader);
