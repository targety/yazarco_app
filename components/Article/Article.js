import React from 'react'
import { View, StyleSheet} from 'react-native'
import ArticleLeft from './ArticleLeft/ArticleLeft'
import ArticleRight from './ArticleRight/ArticleRight'

const Article = (props) => {
    return (
        <View style={styles.articleContainer}>
            <ArticleLeft isFavorite={props.isFavorite} data={props.data} />
            <ArticleRight isFavorite={props.isFavorite} isBookmark={props.isBookmark} token={props.token} removeFavorite={props.removeFavorite} addFavorite={props.addFavorite} removeBookmark={props.removeBookmark} addBookmark={props.addBookmark} shareUrl={props.shareUrl} data={props.data} />
        </View>
    )
}

const styles = StyleSheet.create({
    articleContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        paddingHorizontal: 15,
        marginTop: 10
    },
})

export default Article
