import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation';
import {AUTHOR_IMAGE_ROOT_URL, PUBLISHER_IMAGE_ROOT_URL, BLANK_AUTHOR_IMAGE_URL} from '../../../store/constants';

const ArticleLeft = (props) => {
    return (
        <View style={styles.articleLeftContainer}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => props.navigation.navigate('AuthorDetail', {id: props.data.author.id, name: props.data.author.name, image: props.data.author.image})}>
                <Image 
                    defaultSource={require('../../../images/blank.png')}
                    source={{uri: AUTHOR_IMAGE_ROOT_URL + props.data.author.image}}
                    style={styles.authorImage}  />
            </TouchableOpacity>
            <TouchableOpacity hitSlop={{top: 0, bottom: 20, left: 20, right: 20}} activeOpacity={0.5} onPress={() => props.navigation.navigate('NewspaperDetail', {id: props.data.publisher.id, name: props.data.publisher.name, image: props.data.publisher.image})}>
            <Image 
                source={{uri: PUBLISHER_IMAGE_ROOT_URL + props.data.publisher.image}}
                style={styles.publisherImage}  />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    articleLeftContainer: {
        width: 70,
        alignItems: 'center',
    },
    authorImage: {
      height: 70,
      width: 70,
      padding: 5,
      borderWidth:2,
      borderColor: '#edf2f7',
      borderRadius: 35,
      backgroundColor: '#ffffff'
    },
    publisherImage: {
        marginTop: 10,
        width: 60,
        height: 20,
        resizeMode: 'contain'
    },
})

export default withNavigation(ArticleLeft)
