import React from 'react'
import { FlatList, TouchableHighlight } from 'react-native'
import Article from '../Article/Article'
import { withNavigation } from 'react-navigation'

const ProfileBookmarks = (props) => {
    return (
        <FlatList 
            keyExtractor={(item) => item.id.toString()}
            data={props.data} 
            renderItem={({item}) => 
            (
            <TouchableHighlight 
            underlayColor='#f8f8f8' 
            onPress={() => props.navigation.navigate('Article', {id: item.id, article_url: item.article_url})}>
                <Article 
                shareUrl={props.shareUrl}
                isFavorite={props.favorites.includes(item.id)} 
                addFavorite={props.isAuthenticated ? props.addFavorite : redirectLogin} 
                removeFavorite={props.isAuthenticated ? props.removeFavorite: redirectLogin}
                isBookmark={props.bookmarks.includes(item.id)} 
                addBookmark={props.isAuthenticated ? props.addBookmark : redirectLogin} 
                removeBookmark={props.isAuthenticated ? props.removeBookmark : redirectLogin}
                data={item} 
                token={props.token}/>
            </TouchableHighlight>
            )} 
            onEndReachedThreshold={0}
            onEndReached={props.handleLoadMoreBookmarkArticles}
            ListFooterComponent={props.renderFooter} />
    )
}

export default withNavigation(ProfileBookmarks)
