import React from 'react'
import { FlatList, TouchableHighlight, View, ActivityIndicator } from 'react-native'
import AuthorComponent from '../../components/Author/Author'
import { withNavigation } from 'react-navigation'

const ProfileFollowings = (props) => {
    return (
        <FlatList 
            keyExtractor={(item) => item.id.toString()}
            data={props.data} 
            renderItem={({item}) => 
            (<TouchableHighlight 
                underlayColor='#f8f8f8'
                onPress={() => props.navigation.navigate('AuthorDetail', {id: item.id, name: item.name, image: item.image})}>
                <AuthorComponent 
                data={item}
                isFollowing={props.followings.includes(item.id)}
                followAuthor={props.isAuthenticated ? props.followAuthor : props.redirectLogin}
                unfollowAuthor={props.isAuthenticated ? props.unfollowAuthor : props.redirectLogin}
                token={props.token} />
            </TouchableHighlight>)}
            onEndReachedThreshold={0}
            onEndReached={props.handleLoadMoreFollowing}
            ListFooterComponent={props.renderFooter}
            />
    )
}

export default withNavigation(ProfileFollowings)
