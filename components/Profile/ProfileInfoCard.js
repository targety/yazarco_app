import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const ProfileInfoCard = (props) => {
    return (
        <View style={{flexDirection: 'column', paddingVertical: 15,}}>
            <View style={styles.profileInfoContainer}>
                <View style={styles.profileInfo}>
                    <Text style={styles.profileInfoTitle}>{props.followings}</Text>
                    <Text style={styles.profileInfoDescription}>Takip Edilen</Text>
                </View>
                <View style={styles.profileInfo}>
                    <Text style={styles.profileInfoTitle}>{props.favorites}</Text>
                    <Text style={styles.profileInfoDescription}>Favori</Text>
                </View>
                <View style={styles.profileInfo}>
                    <Text style={styles.profileInfoTitle}>{props.bookmarks}</Text>
                    <Text style={styles.profileInfoDescription}>Okunacak</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    profileInfo: {
        flex: 1,
        alignItems: 'center',
    },
    profileInfoTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1a202c'
    },
    profileInfoDescription: {
        fontSize: 11,
        fontWeight: '700',
        color: '#8c8c8c'
    },
})

export default ProfileInfoCard
