import React from 'react'
import { View, StyleSheet, TouchableHighlight,  } from 'react-native'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
MIcon.loadFont();

const ProfileNavigation = (props) => {
    return (
        <View style={styles.tabContainer}>
            <TouchableHighlight 
            style={styles.tabButtonContainerWithRightBorder}
            underlayColor='#f8f8f8' 
            onPress={(val) => props.onSelect('followings')} >
                <MIcon 
                style={styles.tabButtonIcon}
                name={props.selectedTab == 'followings' ? 'account-check' : 'account-check-outline'}
                color="#8c8c8c"
                size={24} />
            </TouchableHighlight>
            <TouchableHighlight 
            style={styles.tabButtonContainerWithRightBorder}
            underlayColor='#f8f8f8' 
            onPress={(val) => props.onSelect('favorites')} >
                <MIcon 
                style={styles.tabButtonIcon}
                name={props.selectedTab == 'favorites' ? 'heart' : 'heart-outline'}
                color="#8c8c8c"
                size={24} />
            </TouchableHighlight>
            <TouchableHighlight 
            style={styles.tabButtonContainer}
            underlayColor='#f8f8f8' 
            onPress={(val) => props.onSelect('bookmarks')} >
                <MIcon 
                style={styles.tabButtonIcon}
                name={props.selectedTab == 'bookmarks' ? 'bookmark' : 'bookmark-outline'}
                color="#8c8c8c"
                size={24} />
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    tabContainer: {
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: '#e5e5e5',
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
    },
    tabButtonContainer: {
        flex: 1,
        alignItems: 'center'
    },
    tabButtonContainerWithRightBorder: {
        flex: 1,
        alignItems: 'center',
        borderRightWidth: 1,
        borderRightColor: '#e5e5e5',
    },
    tabButtonIcon: {
        paddingVertical: 8
    },
})

export default ProfileNavigation
