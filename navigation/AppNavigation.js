import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from '../screens/Home';
import Explore from '../screens/Explore';
import Newspaper from '../screens/Newspaper';
import NewspaperDetail from '../screens/NewspaperDetail';
import Article from '../screens/Article';
import Author from '../screens/Author';
import AuthorDetail from '../screens/AuthorDetail';
import Profile from '../screens/Profile';
import MainSearch from '../screens/Search/MainSearch';
import DateSearch from '../screens/Search/DateSearch';
import PublisherSearch from '../screens/Search/PublisherSearch';
import Login from '../screens/Login';
import Register from '../screens/Register';
import CustomSidebarMenu from './CustomSidebarMenu'
Icon.loadFont();

const HomeStack = createStackNavigator({
  Home: Home,
  Article: Article,
  AuthorDetail: AuthorDetail,
  NewspaperDetail: NewspaperDetail,
  DateSearch: DateSearch,
  MainSearch: MainSearch
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f8f8f8',
    },
    headerTintColor: '#000000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerBackTitle: null
  },
  headerLeftContainerStyle:{
    paddingLeft: 50
  }
});

const ExploreStack = createStackNavigator({
  Explore: Explore,
  Article: Article,
  AuthorDetail: AuthorDetail,
  NewspaperDetail: NewspaperDetail,
  DateSearch: DateSearch,
  MainSearch: MainSearch
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f8f8f8',
    },
    headerTintColor: '#000000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerBackTitle: null
  },
});

const NewspaperStack = createStackNavigator({
  Newspaper: Newspaper,
  Article: Article,
  AuthorDetail: AuthorDetail,
  NewspaperDetail: NewspaperDetail,
  DateSearch: DateSearch,
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f8f8f8',
    },
    headerTintColor: '#000000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerBackTitle: null
  },
});

const AuthorStack = createStackNavigator({
  Author: Author,
  Article: Article,
  AuthorDetail: AuthorDetail,
  NewspaperDetail: NewspaperDetail,
  DateSearch: DateSearch,
  PublisherSearch: PublisherSearch,
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f8f8f8',
    },
    headerTintColor: '#000000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerBackTitle: null
  },
});

const ProfileStack = createStackNavigator({
  Profile: Profile,
  Article: Article,
  AuthorDetail: AuthorDetail,
  NewspaperDetail: NewspaperDetail,
  DateSearch: DateSearch,
}, {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#f8f8f8',
    },
    headerTintColor: '#000000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerBackTitle: null,
  },
});

const AppNavigation = createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    Explore: { screen: ExploreStack },
    Newspaper: { screen: NewspaperStack },
    Author: { screen: AuthorStack },
    Profile: { screen: ProfileStack }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        if (routeName === 'Home') {
            iconName = 'home';
        } else if (routeName === 'Explore') {
            iconName = 'compass';
        } else if (routeName === 'Newspaper') {
            iconName = 'newspaper';
        } else if (routeName === 'Author') {
            iconName = 'fountain-pen-tip';
        } else if (routeName === 'Profile') {
            iconName = 'account';
        }

        return <Icon name={iconName} size={24} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#555555',
      inactiveTintColor: '#cccccc',
      showLabel: false,
      style: {
        backgroundColor: '#f8f8f8',
      },
    },
  }
)

const LoginStack = createStackNavigator(
  {Login: Login},
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f8f8f8',
      },
      headerTintColor: '#000000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerBackTitle: null
    },
  }
)

const MainDrawer = createDrawerNavigator(
  {
    Main: { screen: AppNavigation },
    Login: {screen: LoginStack},
    Register: {screen: Register}
  },
  {
    contentComponent: CustomSidebarMenu,
  })

export default MainDrawer;