import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, AsyncStorage } from 'react-native'
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
Icon.loadFont();

class CustomSidebarMenu extends Component {

    componentDidMount() {
        this._getToken()
    }

    _getToken = async () => {
        const userToken = await AsyncStorage.getItem('token');
        if(userToken) {
            this.props.authCheck(userToken)
        }
      };

    render() {
        return (
            <SafeAreaView style={styles.sideMenuContainer}>
                <Image
                    defaultSource={require('../images/blank.png')}
                    source={{uri: 'http://yazar.s3.eu-west-1.amazonaws.com/authors/blank.png'}}
                    style={styles.sideMenuProfileIcon}
                    />
                <View
                    style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: '#e2e2e2',
                        marginVertical: 15,
                    }}
                />
                

                <TouchableOpacity style={styles.menuItem} onPress={() => {this.props.navigation.navigate('Main')}}>
                    <Icon 
                    name="home"
                    color="#424242"
                    backgroundColor="#fff"
                    size={20} />
                    <Text style={styles.menuLabel}>Ana Sayfa</Text>
                </TouchableOpacity>
                {!this.props.isAuthenticated &&
                (
                <React.Fragment>
                    <TouchableOpacity style={styles.menuItem} onPress={() => {this.props.navigation.navigate('Login')}}>
                        <Icon 
                        name="login"
                        color="#424242"
                        backgroundColor="#fff"
                        size={20} />
                        <Text style={styles.menuLabel}>Giriş</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.menuItem} onPress={() => {this.props.navigation.navigate('Register')}}>
                        <Icon 
                        name="account"
                        color="#424242"
                        backgroundColor="#fff"
                        size={20} />
                        <Text style={styles.menuLabel}>Kaydol</Text>
                    </TouchableOpacity>
                </React.Fragment>
                )}
                {this.props.isAuthenticated && 
                (
                    <React.Fragment>
                        <TouchableOpacity style={styles.menuItem} onPress={() => {this.props.onLogout()}}>
                            <Icon 
                            name="logout"
                            color="#424242"
                            backgroundColor="#fff"
                            size={20} />
                            <Text style={styles.menuLabel}>Çıkış</Text>
                        </TouchableOpacity>
                    </React.Fragment>
                )}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    sideMenuContainer: {
      flex:1,
      backgroundColor: '#fff',
      alignItems: 'center',
      paddingTop: 20,
    },
    sideMenuProfileIcon: {
      marginTop: 20,
      width: 150,
      height: 150,
      borderRadius: 75
    },
    menuItem: {
        flexDirection: 'row', 
        alignItems: 'flex-start', 
        width: '100%',
        padding: 10
    },
    menuLabel: {
        fontSize: 17,
        color: 'black',
        marginLeft: 10,
    }
  });

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
      authCheck: (token) => dispatch(actions.authCheckState(token)),
      onLogout: () => dispatch(actions.logout())
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(CustomSidebarMenu)