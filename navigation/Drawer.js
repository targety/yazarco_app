import React, { Component } from 'react'
import { TouchableHighlight, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
Icon.loadFont();

export default class Drawer extends Component {
    toggleDrawer = () => {
        this.props.navigationProps.toggleDrawer();
    };

    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
              <TouchableHighlight 
              hitSlop={{top: 20, bottom: 10, left: 10, right: 20}}
              style={{paddingHorizontal: 15}} 
              underlayColor='#f8f8f8' 
              onPress={this.toggleDrawer.bind(this)}>
              <Icon
                name="md-menu"
                color="#000"
                size={24}
                />
              </TouchableHighlight>
            </View>
          );
    }
}
