import { createAppContainer } from 'react-navigation';
import AppNavigation from './AppNavigation';

const AppContainer = createAppContainer(AppNavigation);

export default AppContainer;

// import React, { Component } from 'react';
// import { createAppContainer } from 'react-navigation';
// import AppNavigation from './AppNavigation';

// const AppContainer = createAppContainer(AppNavigation);

// class MainContainer extends Component {
//     render() {
//         return (<AppContainer />)
//     }
// }

// export default MainContainer;