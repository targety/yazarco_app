import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api.yazar.co/v1/'
});

export default instance;