import * as actionTypes from './actionTypes';
import axios from '../../axios';
import {AsyncStorage} from 'react-native';

export const loginStart  = () => {
    return {
        type: actionTypes.LOGIN_START
    };
}

export const loginSuccess = (token) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        token: token
    };
}

export const loginFail = (error) => {
    return {
        type: actionTypes.LOGIN_FAIL,
        error: error
    };
}

export const logout = () => {
    AsyncStorage.removeItem('token');
    return {
        type: actionTypes.LOGOUT
    }
}

export const login = (email, password) => {
    return dispatch => {
        dispatch(loginStart());
        const loginData = {
            email: email,
            password: password
        }
        axios.post('/login', loginData)
        .then(response => {
            _setToken(response.data.access_token)
            dispatch(loginSuccess(response.data.access_token));
        })
        .catch(error => {
            dispatch(loginFail(error));
        });
    };
}

export const authCheckState = (token) => {
    return dispatch => {
        if (!token) {
            dispatch(logout());
        } else {
            dispatch(loginSuccess(token));
        }
    }
}

_setToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
      alert(error)
    }
};
