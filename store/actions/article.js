import * as actionTypes from './actionTypes';
import axios from '../../axios';
import {AsyncStorage} from 'react-native';

export const fetchArticlesStart  = () => {
    return {
        type: actionTypes.FETCH_ARTICLES_START
    };
}

export const fetchArticlesSuccess = (articles) => {
    return {
        type: actionTypes.FETCH_ARTICLES_SUCCESS,
        articles: articles
    };
}

export const fetchArticlesFail = (error) => {
    return {
        type: actionTypes.FETCH_ARTICLES_FAIL,
        error: error
    };
}

export const fetchArticles = (publish_date) => {
    return dispatch => {
        dispatch(fetchArticlesStart());
        axios.get('/articles?publish_date=' + publish_date)
        .then(response => {
            dispatch(fetchArticlesSuccess(response.data.data));
        })
        .catch(error => {
            dispatch(fetchArticlesFail(error));
        });
    };
}

export const setFavorites = (favorites) => {
    return {
        type: actionTypes.SET_FAVORITES,
        favorites: favorites
    };
}

export const fetchFavorites = (token) => {
    return dispatch => {
        axios.get('/users/favorites/all', {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(setFavorites(response.data))
        })
        .catch(error => {
            dispatch(setFavorites([]))
        });
    };
}

export const addFavorite = (article_id, token) => {
    return dispatch => {
        axios.post('/users/favorites', {article_id: article_id}, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchFavorites(token));
        })
        .catch(error => {
        });
    };
}

export const removeFavorite= (article_id, token) => {
    return dispatch => {
        axios.delete('/users/favorites/' + article_id, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchFavorites(token));
        })
        .catch(error => {
        });
    };
}

export const setBookmarks = (bookmarks) => {
    return {
        type: actionTypes.SET_BOOKMARKS,
        bookmarks: bookmarks
    };
}

export const fetchBookmarks = (token) => {
    return dispatch => {
        axios.get('/users/lists/all', {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(setBookmarks(response.data))
        })
        .catch(error => {
            dispatch(setBookmarks([]))
        });
    };
}

export const addBookmark = (article_id, token) => {
    return dispatch => {
        axios.post('/users/lists', {article_id: article_id}, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchBookmarks(token));
        })
        .catch(error => {
        });
    };
}

export const removeBookmark= (article_id, token) => {
    return dispatch => {
        axios.delete('/users/lists/' + article_id, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchBookmarks(token));
        })
        .catch(error => {
        });
    };
}

export const readArticle = (article_id) => {
    return dispatch => {
        axios.put('/articles/' + article_id + '/read')
        .then(response => {
        })
        .catch(error => {
        });
    };
}