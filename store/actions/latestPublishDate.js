import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchLatestPublishDateStart  = () => {
    return {
        type: actionTypes.FETCH_LATEST_PUBLISH_DATE_START
    };
}

export const fetchLatestPublishDateSuccess = (publish_date) => {
    return {
        type: actionTypes.FETCH_LATEST_PUBLISH_DATE_SUCCESS,
        publish_date: publish_date
    };
}

export const fetchLatestPublishDateFail = (error) => {
    return {
        type: actionTypes.FETCH_LATEST_PUBLISH_DATE_FAIL,
        error: error
    };
}

export const fetchLatestPublishDate = () => {
    return dispatch => {
        dispatch(fetchLatestPublishDateStart());
        axios.get('/articles/latestPublishDate')
        .then(response => {
            dispatch(fetchLatestPublishDateSuccess(response.data.publish_date));
        })
        .catch(error => {
            dispatch(fetchLatestPublishDateFail(error));
        });
    };
}

export const updatePublishDate = (publish_date) => {
    return {
        type: actionTypes.UPDATE_PUBLISH_DATE,
        publish_date: publish_date
    };
}

