import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchAuthorsStart  = () => {
    return {
        type: actionTypes.FETCH_AUTHORS_START
    };
}

export const fetchAuthorsSuccess = (authors) => {
    return {
        type: actionTypes.FETCH_AUTHORS_SUCCESS,
        authors: authors
    };
}

export const fetchAuthorsFail = (error) => {
    return {
        type: actionTypes.FETCH_AUTHORS_FAIL,
        error: error
    };
}

export const fetchAuthorArticlesStart  = () => {
    return {
        type: actionTypes.FETCH_AUTHOR_ARTICLES_START
    };
}

export const fetchAuthorArticlesSuccess = (articles) => {
    return {
        type: actionTypes.FETCH_AUTHOR_ARTICLES_SUCCESS,
        articles: articles
    };
}

export const fetchAuthorArticlesFail = (error) => {
    return {
        type: actionTypes.FETCH_AUTHOR_ARTICLES_FAIL,
        error: error
    };
}

export const fetchAuthors = (page, publishers) => {
    return dispatch => {
        dispatch(fetchAuthorsStart());
        axios.get('/authors?page=' + page + '&publishers=' + publishers)
        .then(response => {
            dispatch(fetchAuthorsSuccess(response.data.data));
        })
        .catch(error => {
            dispatch(fetchAuthorsFail(error));
        });
    };
}

export const fetchAuthorArticles = (page, author_id) => {
    return dispatch => {
        dispatch(fetchAuthorArticlesStart());
        axios.get('/authors/' + author_id + '/articles?page=' + page)
        .then(response => {
            dispatch(fetchAuthorArticlesSuccess(response.data.data));
            dispatch(setNextPageStatus((response.data.links.next) ? true : false))
        })
        .catch(error => {
            dispatch(fetchAuthorArticlesFail(error));
        });
    };
}

export const setNextPageStatus = (status) => {
    return {
        type: actionTypes.SET_NEXT_PAGE_STATUS,
        status: status
    };
}

export const setFollowings = (followings) => {
    return {
        type: actionTypes.SET_FOLLOWINGS,
        followings: followings
    };
}

export const fetchFollowings = (token) => {
    return dispatch => {
        dispatch(fetchAuthorsStart());
        axios.get('/users/followings/all', {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(setFollowings(response.data))
        })
        .catch(error => {
            dispatch(setFollowings([]))
        });
    };
}

export const followAuthor = (author_id, token) => {
    return dispatch => {
        axios.post('/users/followings', {author_id: author_id}, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchFollowings(token));
        })
        .catch(error => {
            dispatch(setFollowings([]))
        });
    };
}

export const unfollowAuthor = (author_id, token) => {
    return dispatch => {
        axios.delete('/users/followings/' + author_id, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchFollowings(token));
        })
        .catch(error => {
            dispatch(setFollowings([]))
        });
    };
}