import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchUserFollowingAuthorsStart  = () => {
    return {
        type: actionTypes.FETCH_USER_FOLLOWING_AUTHORS_START
    };
}

export const fetchUserFollowingAuthorsSuccess = (authors) => {
    return {
        type: actionTypes.FETCH_USER_FOLLOWING_AUTHORS_SUCCESS,
        authors: authors
    };
}

export const fetchUserFollowingAuthorsFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_FOLLOWING_AUTHORS_FAIL,
        error: error
    };
}

export const fetchUserFollowingAuthors = (page, token) => {
    return dispatch => {
        dispatch(fetchUserFollowingAuthorsStart());
        axios.get('/users/followings?page=' + page, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchUserFollowingAuthorsSuccess(response.data.data));
            dispatch(setNextPageStatus((response.data.links.next) ? true : false))
        })
        .catch(error => {
            dispatch(fetchUserFollowingAuthorsFail(error));
        });
    };
}

export const setNextPageStatus = (status) => {
    return {
        type: actionTypes.SET_NEXT_PAGE_STATUS,
        status: status
    };
}

export const fetchUserFavoriteArticlesStart  = () => {
    return {
        type: actionTypes.FETCH_USER_FAVORITE_ARTICLES_START
    };
}

export const fetchUserFavoriteArticlesSuccess = (articles) => {
    return {
        type: actionTypes.FETCH_USER_FAVORITE_ARTICLES_SUCCESS,
        articles: articles
    };
}

export const fetchUserFavoriteArticlesFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_FAVORITE_ARTICLES_FAIL,
        error: error
    };
}

export const fetchUserFavoriteArticles = (page, token) => {
    return dispatch => {
        dispatch(fetchUserFavoriteArticlesStart());
        axios.get('/users/favorites?page=' + page, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchUserFavoriteArticlesSuccess(response.data.data));
            dispatch(setNextPageStatus((response.data.links.next) ? true : false))
        })
        .catch(error => {
            dispatch(fetchUserFavoriteArticlesFail(error));
        });
    };
}

export const fetchUserBookmarkArticlesStart  = () => {
    return {
        type: actionTypes.FETCH_USER_BOOKMARK_ARTICLES_START
    };
}

export const fetchUserBookmarkArticlesSuccess = (articles) => {
    return {
        type: actionTypes.FETCH_USER_BOOKMARK_ARTICLES_SUCCESS,
        articles: articles
    };
}

export const fetchUserBookmarkArticlesFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_BOOKMARK_ARTICLES_FAIL,
        error: error
    };
}

export const fetchUserBookmarkArticles = (page, token) => {
    return dispatch => {
        dispatch(fetchUserBookmarkArticlesStart());
        axios.get('/users/lists?page=' + page, {
            headers: {'Authorization': 'Bearer ' + token}
        })
        .then(response => {
            dispatch(fetchUserBookmarkArticlesSuccess(response.data.data));
            dispatch(setNextPageStatus((response.data.links.next) ? true : false))
        })
        .catch(error => {
            dispatch(fetchUserBookmarkArticlesFail(error));
        });
    };
}