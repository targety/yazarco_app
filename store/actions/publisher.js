import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchPublishersStart  = () => {
    return {
        type: actionTypes.FETCH_PUBLISHERS_START
    };
}

export const fetchPublishersSuccess = (publishers) => {
    return {
        type: actionTypes.FETCH_PUBLISHERS_SUCCESS,
        publishers: publishers
    };
}

export const fetchPublishersFail = (error) => {
    return {
        type: actionTypes.FETCH_PUBLISHERS_FAIL,
        error: error
    };
}

export const fetchPublishers = () => {
    return dispatch => {
        dispatch(fetchPublishersStart());
        axios.get('/publishers')
        .then(response => {
            dispatch(fetchPublishersSuccess(response.data.data));
            dispatch(setDisabledPublishers());
            dispatch(setFilteredPublishers());
        })
        .catch(error => {
            dispatch(fetchPublishersFail(error));
        });
    };
}

export const togglePublisher  = (publisher_id) => {
    return {
        type: actionTypes.TOGGLE_PUBLISHER,
        publisher_id: publisher_id
    };
}

export const setDisabledPublishers  = () => {
    return {
        type: actionTypes.SET_DISABLED_PUBLISHERS
    };
}

export const setFilteredPublishers  = () => {
    return {
        type: actionTypes.SET_FILTERED_PUBLISHERS
    };
}

export const filterPublisher = (publisher_id) => {
    return dispatch => {
        dispatch(togglePublisher(publisher_id));
        dispatch(setDisabledPublishers());
        dispatch(setFilteredPublishers());
    };
}