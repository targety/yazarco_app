import { combineReducers } from 'redux';
import latestPublishDateReducer from './latestPublishDate';
import articleReducer from './article';
import publisherReducer from './publisher';
import authReducer from './auth';
import authorReducer from './author';
import userReducer from './user';

export default rootReducer = combineReducers({
    latestPublishDate: latestPublishDateReducer,
    article: articleReducer,
    publisher: publisherReducer,
    auth: authReducer,
    author: authorReducer,
    user: userReducer
});