import * as actionTypes from '../actions/actionTypes';
import { List } from 'immutable'

const initialState = {
    loading: false,
    authors: [],
    favorite_articles: [],
    bookmark_articles: [],
    followings: List(),
    hasNextPage: true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_FOLLOWING_AUTHORS_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_USER_FOLLOWING_AUTHORS_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_USER_FOLLOWING_AUTHORS_SUCCESS:
            return {
                ...state,
                authors: action.authors,
                loading: false
            }
        case actionTypes.FETCH_USER_FAVORITE_ARTICLES_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_USER_FAVORITE_ARTICLES_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_USER_FAVORITE_ARTICLES_SUCCESS:
            return {
                ...state,
                favorite_articles: action.articles,
                loading: false
            }
        case actionTypes.FETCH_USER_BOOKMARK_ARTICLES_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_USER_BOOKMARK_ARTICLES_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_USER_BOOKMARK_ARTICLES_SUCCESS:
            return {
                ...state,
                bookmark_articles: action.articles,
                loading: false
            }
        case actionTypes.SET_NEXT_PAGE_STATUS:
            return {
                ...state,
                hasNextPage: action.status
            }
        default:
            return state;
    }
}

export default reducer;