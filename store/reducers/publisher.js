import * as actionTypes from '../actions/actionTypes';
import _ from 'lodash';

const initialState = {
    loading: false,
    publishers: [],
    selectedNewspaper: null,
    disabledPublishers: [],
    filteredPublishers: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PUBLISHERS_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_PUBLISHERS_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_PUBLISHERS_SUCCESS:
            // return {
            //     ...state,
            //     publishers: action.publishers,
            //     loading: false
            // }
            return Object.assign({}, state, {
                publishers: action.publishers.map((publisher) => {
                  if (state.disabledPublishers.includes(publisher.id)) {
                    return Object.assign({}, publisher, {
                      is_visible: false
                    })
                  }
                  return publisher
                }),
                loading: false
              })
        case actionTypes.TOGGLE_PUBLISHER:
            return Object.assign({}, state, {
                publishers: state.publishers.map((publisher) => {
                  if (publisher.id === action.publisher_id) {
                    return Object.assign({}, publisher, {
                      is_visible: !publisher.is_visible
                    })
                  }
                  return publisher
                })
              })
        case actionTypes.SET_DISABLED_PUBLISHERS:
            return Object.assign({}, state, {
                disabledPublishers: _(state.publishers).filter(publisher => !publisher.is_visible).map('id').value()
                })
        case actionTypes.SET_FILTERED_PUBLISHERS:
            return Object.assign({}, state, {
                filteredPublishers: _(state.publishers).filter(publisher => publisher.is_visible).map('id').value()
                })
        case actionTypes.SELECT_NEWSPAPER:
            return {
                ...state,
                selectedNewspaper: action.newspaper
            }
        case actionTypes.SINGLE_SELECT:
            return Object.assign({}, state, {
                publishers: state.publishers.map((publisher) => {
                    if (publisher.slug !== action.publisher) {
                    return Object.assign({}, publisher, {
                        is_visible: false
                    })
                    }
                    return publisher
                })
                })
        default:
            return state;
    }
}

export default reducer;