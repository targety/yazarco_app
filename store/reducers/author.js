import * as actionTypes from '../actions/actionTypes';
import { List } from 'immutable'

const initialState = {
    loading: false,
    authors: [],
    articles: [],
    followings: List(),
    hasNextPage: true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_AUTHORS_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_AUTHORS_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_AUTHORS_SUCCESS:
            return {
                ...state,
                authors: action.authors,
                loading: false
            }
        case actionTypes.FETCH_AUTHOR_ARTICLES_START:
            return {
                ...state,
                loading: true,
                hasNextPage: true
            }
        case actionTypes.FETCH_AUTHOR_ARTICLES_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_AUTHOR_ARTICLES_SUCCESS:
            return {
                ...state,
                articles: action.articles,
                loading: false
            }
        case actionTypes.SET_FOLLOWINGS:
            return {
                ...state,
                followings: List(action.followings),
                loading: false
            }
        case actionTypes.SET_NEXT_PAGE_STATUS:
            return {
                ...state,
                hasNextPage: action.status
            }
        default:
            return state;
    }
}

export default reducer;