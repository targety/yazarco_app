import * as actionTypes from '../actions/actionTypes';
import { List } from 'immutable'
 
const initialState = {
    loading: false,
    articles: [],
    favorites: List(),
    bookmarks: List()
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ARTICLES_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_ARTICLES_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_ARTICLES_SUCCESS:
            return {
                ...state,
                articles: action.articles,
                loading: false
            }
        case actionTypes.SET_FAVORITES:
            return {
                ...state,
                favorites: List(action.favorites)
            }
        case actionTypes.SET_BOOKMARKS:
            return {
                ...state,
                bookmarks: List(action.bookmarks)
            }
        default:
            return state;
    }
}

export default reducer;