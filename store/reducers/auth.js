import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: false,
    token: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                token: action.token,
                loading: false
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                token: null
            }
        default:
            return state;
    }
}

export default reducer;