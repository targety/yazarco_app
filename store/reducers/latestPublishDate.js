import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    publish_date: new Date().toISOString().slice(0,10)
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_LATEST_PUBLISH_DATE_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_LATEST_PUBLISH_DATE_FAIL:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_LATEST_PUBLISH_DATE_SUCCESS:
            return {
                ...state,
                publish_date: action.publish_date,
                loading: false
            }
        case actionTypes.UPDATE_PUBLISH_DATE:
            return {
                ...state,
                publish_date: action.publish_date
            }
        default:
            return state;
    }
}

export default reducer;