import React from 'react';
import { Provider } from 'react-redux';
import store from './store/index';
import AppContainer from './navigation/index';

export default function App() {
  return (<Provider store={store}><AppContainer /></Provider>)
}