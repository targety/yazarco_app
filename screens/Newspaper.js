import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator, FlatList, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../store/actions/index'
import Drawer from '../navigation/Drawer'
import Publisher from '../components/Publisher/Publisher'

class Newspaper extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Gazeteler',
            headerLeft: <Drawer navigationProps={navigation} />,
        }
    };

    componentDidMount() {
        this.props.onFetchPublishers()
    }

    render() {
        return (
            <View style={styles.container}>
                {
                this.props.loading ? 
                (<ActivityIndicator size="large" color="#000" />) : 
                (
                    <FlatList 
                    keyExtractor={(item) => item.id.toString()}
                    data={this.props.publishers} 
                    renderItem={({item}) => 
                    (<TouchableHighlight 
                        activeOpacity={0.5} 
                        underlayColor='#f8f8f8'
                        onPress={() => this.props.navigation.navigate('NewspaperDetail', {id: item.id, name: item.name, image: item.image})}>
                        <Publisher data={item} />
                    </TouchableHighlight>)} />
                )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
    }
})

const mapStateToProps = state => {
    return {
        loading: state.publisher.loading,
        publishers: state.publisher.publishers
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchPublishers: () => dispatch(actions.fetchPublishers()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Newspaper)
