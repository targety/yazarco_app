import React, { Component } from 'react'
import { View, StyleSheet, TouchableHighlight, ActivityIndicator, Text, Alert } from 'react-native'
import Drawer from '../navigation/Drawer'
import { connect } from 'react-redux'
import * as actions from '../store/actions/index'
import ProfileInfoCard from '../components/Profile/ProfileInfoCard'
import ProfileFollowings from '../components/Profile/ProfileFollowings'
import ProfileFavorites from '../components/Profile/ProfileFavorites'
import ProfileBookmarks from '../components/Profile/ProfileBookmarks'
import ProfileNavigation from '../components/Profile/ProfileNavigation'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
MIcon.loadFont();

class Profile extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Profil',
            headerLeft: <Drawer navigationProps={navigation} />,
        }
    };

    state = {
        selectedTab: 'followings',
        authors: [],
        favoriteArticles: [],
        bookmarkArticles: [],
        followingPage: 1,
        favoritePage: 1,
        bookmarkPage: 1
    }

    componentDidMount() {
        if(this.state.selectedTab == 'followings') {
            this.props.onFetchUserFollowingAuthors(this.state.followingPage, this.props.token)
        }
        this.props.onFetchFollowings(this.props.token)
        this.props.onFetchFavorites(this.props.token)
        this.props.onFetchBookmarks(this.props.token)
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.selectedTab !== prevState.selectedTab) {
            if (this.state.selectedTab == 'favorites' && this.state.favoriteArticles.length === 0) {
                this.props.onFetchUserFavoriteArticles(this.state.favoritePage, this.props.token)
            }

            if (this.state.selectedTab == 'bookmarks' && this.state.bookmarkArticles.length === 0) {
                this.props.onFetchUserBookmarkArticles(this.state.bookmarkPage, this.props.token)
            }
        }

        if(this.props.bookmarkArticles !== prevProps.bookmarkArticles) {
            this.setState({
                bookmarkArticles: this.state.bookmarkArticles.concat(this.props.bookmarkArticles)
            });
        }

        if(this.props.favoriteArticles !== prevProps.favoriteArticles) {
            this.setState({
                favoriteArticles: this.state.favoriteArticles.concat(this.props.favoriteArticles)
            });
        }

        if(this.props.authors !== prevProps.authors) {
            this.setState({
                authors: this.state.authors.concat(this.props.authors)
            });
        }

        if(this.state.followingPage !== prevState.followingPage) {
            this.props.onFetchUserFollowingAuthors(this.state.followingPage, this.props.token)
        }

        if(this.state.favoritePage !== prevState.favoritePage) {
            this.props.onFetchUserFavoriteArticles(this.state.favoritePage, this.props.token)
        }

        if(this.state.bookmarkPage !== prevState.bookmarkPage) {
            this.props.onFetchUserBookmarkArticles(this.state.bookmarkPage, this.props.token)
        }
    }

    handleLoadMoreFollowing = () => {
        if(this.props.hasNextPage) {
            this.setState({
                followingPage: this.state.followingPage + 1
            })
        }
    }

    handleLoadMoreFavoriteArticles = () => {
        if(this.props.hasNextPage) {
            this.setState({
                favoritePage: this.state.favoritePage + 1
            })
        }
    }

    handleLoadMoreBookmarkArticles = () => {
        if(this.props.hasNextPage) {
            this.setState({
                bookmarkPage: this.state.bookmarkPage + 1
            })
        }
    }

    renderFooter = () => {
        if(this.props.hasNextPage) {
            return <View style={{marginVertical: 10}}><ActivityIndicator size="large" color="#000" /></View>
        }
        return <View style={{marginVertical: 10, alignItems: 'center'}}><Text>Liste sonu</Text></View>
    }

    redirectLogin() {
        Alert.alert(
            'Uyarı',
            'Kullanıcı girişi yapmalısınız',
            [
            {
                text: 'İptal',
                style: 'cancel',
                },
              {text: 'Giriş Yap', onPress: () => this.props.navigation.navigate('Login')},
            ],
            {cancelable: false},
          );
    }

    render() {
        let selectedTabComponent = <ProfileFollowings 
        isAuthenticated={this.props.isAuthenticated} 
        token={this.props.token} 
        followAuthor={(id, token) => this.props.onFollowAuthor(id, token)} 
        unfollowAuthor={(id, token) => this.props.onUnfollowAuthor(id, token)} 
        redirectLogin={() => this.redirectLogin()} 
        renderFooter={() => this.renderFooter()} 
        handleLoadMoreFollowing={() => this.handleLoadMoreFollowing()} 
        data={this.state.authors} 
        followings={this.props.followings} />

        if (this.state.selectedTab == 'favorites') {
            selectedTabComponent = <ProfileFavorites
            isAuthenticated={this.props.isAuthenticated}
            token={this.props.token}
            addFavorite={(id, token) => this.props.onAddFavorite(id, token)} 
            removeFavorite={(id, token) => this.props.onRemoveFavorite(id, token)}
            addBookmark={(id, token) => this.props.onAddBookmark(id, token)} 
            removeBookmark={(id, token) => this.props.onRemoveBookmark(id, token)}
            redirectLogin={() => this.redirectLogin()} 
            renderFooter={() => this.renderFooter()}
            data={this.state.favoriteArticles}
            favorites={this.props.favorites}
            bookmarks={this.props.bookmarks}
            handleLoadMoreFavoriteArticles={() => this.handleLoadMoreFavoriteArticles()}  />
        } else if (this.state.selectedTab == 'bookmarks') {
            selectedTabComponent = <ProfileBookmarks
            isAuthenticated={this.props.isAuthenticated}
            token={this.props.token}
            addFavorite={(id, token) => this.props.onAddFavorite(id, token)} 
            removeFavorite={(id, token) => this.props.onRemoveFavorite(id, token)}
            addBookmark={(id, token) => this.props.onAddBookmark(id, token)} 
            removeBookmark={(id, token) => this.props.onRemoveBookmark(id, token)}
            redirectLogin={() => this.redirectLogin()} 
            renderFooter={() => this.renderFooter()}
            data={this.state.bookmarkArticles}
            favorites={this.props.favorites}
            bookmarks={this.props.bookmarks}
            handleLoadMoreBookmarkArticles={() => this.handleLoadMoreBookmarkArticles()}  />
        } else {
            selectedTabComponent = <ProfileFollowings 
            isAuthenticated={this.props.isAuthenticated} 
            token={this.props.token} 
            followAuthor={(id, token) => this.props.onFollowAuthor(id, token)} 
            unfollowAuthor={(id, token) => this.props.onUnfollowAuthor(id, token)} 
            redirectLogin={() => this.redirectLogin()} 
            renderFooter={() => this.renderFooter()} 
            handleLoadMoreFollowing={() => this.handleLoadMoreFollowing()} 
            data={this.state.authors} 
            followings={this.props.followings} />
        }

        return (
            <View style={styles.container}>
                <ProfileInfoCard 
                followings={this.props.followings.size} 
                favorites={this.props.favorites.size}
                bookmarks={this.props.bookmarks.size} />
                <ProfileNavigation 
                selectedTab={this.state.selectedTab} 
                onSelect={(val) => this.setState({selectedTab: val})} />
                {selectedTabComponent}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
    },
})

const mapStateToProps = state => {
    return {
        loading: state.user.loading,
        authors: state.user.authors,
        favoriteArticles: state.user.favorite_articles,
        bookmarkArticles: state.user.bookmark_articles,
        token: state.auth.token,
        isAuthenticated: state.auth.token !== null,
        followings: state.author.followings,
        favorites: state.article.favorites,
        bookmarks: state.article.bookmarks,
        hasNextPage: state.user.hasNextPage
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchUserFollowingAuthors: (page, token) => dispatch(actions.fetchUserFollowingAuthors(page, token)),
        onFetchUserFavoriteArticles: (page, token) => dispatch(actions.fetchUserFavoriteArticles(page, token)),
        onFetchUserBookmarkArticles: (page, token) => dispatch(actions.fetchUserBookmarkArticles(page, token)),
        onFetchFollowings: (token) => dispatch(actions.fetchFollowings(token)),
        onFetchFavorites: (token) => dispatch(actions.fetchFavorites(token)),
        onFetchBookmarks: (token) => dispatch(actions.fetchBookmarks(token)),
        onFollowAuthor: (id, token) => dispatch(actions.followAuthor(id, token)),
        onUnfollowAuthor: (id, token) => dispatch(actions.unfollowAuthor(id, token)),
        onAddFavorite: (id, token) => dispatch(actions.addFavorite(id, token)),
        onRemoveFavorite: (id, token) => dispatch(actions.removeFavorite(id, token)), 
        onAddBookmark: (id, token) => dispatch(actions.addBookmark(id, token)),
        onRemoveBookmark: (id, token) => dispatch(actions.removeBookmark(id, token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
