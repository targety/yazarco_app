import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList , ActivityIndicator, Alert, TouchableHighlight, Share} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import Article from '../components/Article/Article'
import HomeWithUnauthenticated from '../components/Common/Home/HomeWithUnauthenticated'
import HomeWithNoFollow from '../components/Common/Home/HomeWithNoFollow'
import HomeWithNoData from '../components/Common/Home/HomeWithNoData'
import * as actions from '../store/actions/index'
import Drawer from '../navigation/Drawer'
Icon.loadFont();

class Home extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Anasayfa',
            headerLeft: <Drawer navigationProps={navigation} />,
            headerRight: () => (
                <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8' 
                onPress={() => navigation.navigate('DateSearch')} >
                    <Icon
                    name="ios-search"
                    color="#000"
                    size={24}
                    />
                </TouchableHighlight>
            ),
        }
    };

    state = {
        articles: {},
        isReady: false
    }

    componentDidMount() {
        if(this.props.publish_date) {
            this.props.onFetchArticles(this.props.publish_date)

            if(this.props.token) {
                this.props.onFetchFollowings(this.props.token)
                this.props.onFetchFavorites(this.props.token)
                this.props.onFetchBookmarks(this.props.token) 
            }
        }
        this.setState({isReady: true})
    }

    componentDidUpdate(prevProps) {
        if(this.props.publish_date !== prevProps.publish_date) {
            this.props.onFetchArticles(this.props.publish_date);
        }

        if(this.props.articles !== prevProps.articles || this.props.followings !== prevProps.followings) {
            const filteredArticle = this.props.articles.filter(article => this.props.followings.includes(article.author_id));
            this.setState({
                articles: filteredArticle
            });
        }

        if((this.props.token !== prevProps.token) && this.props.token) {
            this.props.onFetchFollowings(this.props.token)
            this.props.onFetchFavorites(this.props.token)
            this.props.onFetchBookmarks(this.props.token)
        }
    }

    redirectLogin() {
        Alert.alert(
            'Uyarı',
            'Kullanıcı girişi yapmalısınız',
            [
            {
                text: 'İptal',
                style: 'cancel',
                },
              {text: 'Giriş Yap', onPress: () => this.props.navigation.navigate('Login')},
            ],
            {cancelable: false},
          );
    }

    shareUrl = async (url) => {
        try {
            const result = await Share.share({
                message: url,
            });
        } catch (error) {
            alert(error.message);
        }
    } 

    render() {
        let authComponent = null

        articles = (<FlatList 
            keyExtractor={(item) => item.id.toString()}
            data={this.state.articles} 
            ListEmptyComponent={<HomeWithNoData/>}
            renderItem={({item}) => 
            (
            <TouchableHighlight 
            underlayColor='#f8f8f8' 
            onPress={() => this.props.navigation.navigate('Article', {id: item.id, article_url: item.article_url})}>
                <Article 
                shareUrl={(url) => (this.shareUrl(url))}
                isFavorite={this.props.favorites.includes(item.id)} 
                addFavorite={(id, token) => this.props.isAuthenticated ? this.props.onAddFavorite(id, token) : this.redirectLogin()} 
                removeFavorite={(id, token) => this.props.isAuthenticated ? this.props.onRemoveFavorite(id, token): this.redirectLogin()}
                isBookmark={this.props.bookmarks.includes(item.id)} 
                addBookmark={(id, token) => this.props.isAuthenticated ? this.props.onAddBookmark(id, token) : this.redirectLogin()} 
                removeBookmark={(id, token) => this.props.isAuthenticated ? this.props.onRemoveBookmark(id, token): this.redirectLogin()}
                data={item} 
                token={this.props.token}/>
            </TouchableHighlight>
            )}/>)

        if(this.props.loading || this.props.author_loading || this.props.auth_loading || !this.state.isReady) {
            authComponent = <ActivityIndicator size="large" color="#000" />
        } else if (!this.props.isAuthenticated) {
            authComponent = <HomeWithUnauthenticated/>
        } else {
            authComponent = articles
        }

        return (
            <View style={styles.container}>
            {authComponent}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    info: {
        paddingTop: 15,
        paddingHorizontal: 20,
        fontSize: 16,
        lineHeight: 24,
        color: '#1a202c',
    },
    button: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#4a5568',
        padding: 10,
        marginTop: 20
    },
    buttonLabel: {
        color: '#fff', 
        fontSize: 18, 
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    return {
        publish_date: state.latestPublishDate.publish_date,
        articles: state.article.articles,
        disabledPublishers: state.publisher.disabledPublishers,
        loading: state.article.loading,
        author_loading: state.author.loading,
        auth_loading: state.auth.loading,
        isAuthenticated: state.auth.token !== null,
        token: state.auth.token,
        favorites: state.auth.token ? state.article.favorites : [],
        bookmarks: state.auth.token ? state.article.bookmarks : [],
        followings: state.author.followings,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchArticles: (publish_date) => dispatch(actions.fetchArticles(publish_date)),
        onAddFavorite: (id, token) => dispatch(actions.addFavorite(id, token)),
        onRemoveFavorite: (id, token) => dispatch(actions.removeFavorite(id, token)),
        onFetchFavorites: (token) => dispatch(actions.fetchFavorites(token)),
        onAddBookmark: (id, token) => dispatch(actions.addBookmark(id, token)),
        onRemoveBookmark: (id, token) => dispatch(actions.removeBookmark(id, token)),
        onFetchBookmarks: (token) => dispatch(actions.fetchBookmarks(token)),
        onFetchFollowings: (token) => dispatch(actions.fetchFollowings(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
