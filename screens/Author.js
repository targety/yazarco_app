import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator, FlatList, TouchableHighlight, Alert, Text } from 'react-native'
import Drawer from '../navigation/Drawer'
import AuthorComponent from '../components/Author/Author'
import { connect } from 'react-redux'
import * as actions from '../store/actions/index'
import Icon from 'react-native-vector-icons/Ionicons';
import {List} from 'immutable'
Icon.loadFont();

class Author extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Yazarlar',
            headerLeft: <Drawer navigationProps={navigation} />,
            headerRight: () => (
                <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8' 
                onPress={() => navigation.navigate('PublisherSearch')} >
                    <Icon
                    name="ios-search"
                    color="#000"
                    size={24}
                    />
                </TouchableHighlight>
            ),
        }
    };

    state = {
        authors: [],
        page: 1,
    }

    componentDidMount() {        
        if(this.state.page) {
            this.props.onFetchAuthors(this.state.page, this.props.filteredPublishers)
            this.props.onFetchFollowings(this.props.token)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.authors !== prevProps.authors) {
            this.setState({
                authors: this.state.authors.concat(this.props.authors)
            });
        }

        if(this.state.page !== prevState.page) {
            this.props.onFetchAuthors(this.state.page, this.props.filteredPublishers)
        }

        if(this.props.filteredPublishers !== prevProps.filteredPublishers) {
            this.setState({
                page: 1,
                authors: []
            })
            this.props.onFetchAuthors(this.state.page, this.props.filteredPublishers)
        }
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1
        })
    }

    renderFooter = () => {
        return <View style={{marginVertical: 10}}><ActivityIndicator size="large" color="#000" /></View>
    }

    redirectLogin() {
        Alert.alert(
            'Uyarı',
            'Kullanıcı girişi yapmalısınız',
            [
            {
                text: 'İptal',
                style: 'cancel',
                },
              {text: 'Giriş Yap', onPress: () => this.props.navigation.navigate('Login')},
            ],
            {cancelable: false},
          );
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    <FlatList 
                    keyExtractor={(item) => item.id.toString()}
                    data={this.state.authors} 
                    renderItem={({item}) => 
                    (<TouchableHighlight 
                        underlayColor='#f8f8f8'
                        onPress={() => this.props.navigation.navigate('AuthorDetail', {id: item.id, name: item.name, image: item.image})}>
                        <AuthorComponent 
                        data={item}
                        isFollowing={this.props.followings.includes(item.id)}
                        followAuthor={(id, token) => this.props.isAuthenticated ? this.props.onFollowAuthor(id, token) : this.redirectLogin()}
                        unfollowAuthor={(id, token) => this.props.isAuthenticated ? this.props.onUnfollowAuthor(id, token) : this.redirectLogin()}
                        token={this.props.token} />
                    </TouchableHighlight>)}
                    onEndReachedThreshold={0}
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}
                    />
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center'
    },
})

const mapStateToProps = state => {
    return {
        loading: state.author.loading,
        authors: state.author.authors,
        filteredPublishers: state.publisher.filteredPublishers,
        followings: state.author.followings,
        token: state.auth.token,
        isAuthenticated: state.auth.token !== null,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchAuthors: (page, publishers) => dispatch(actions.fetchAuthors(page, publishers)),
        onFetchPublishers: () => dispatch(actions.fetchPublishers()),
        onFetchFollowings: (token) => dispatch(actions.fetchFollowings(token)),
        onFollowAuthor: (id, token) => dispatch(actions.followAuthor(id, token)),
        onUnfollowAuthor: (id, token) => dispatch(actions.unfollowAuthor(id, token)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Author)
