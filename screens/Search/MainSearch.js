import React, {Component} from 'react'
import { View, Text, StyleSheet, FlatList, TouchableHighlight } from 'react-native'
import DatePicker from 'react-native-datepicker'
import * as actions from '../../store/actions/index'
import PublisherSwitch from '../../components/PublisherSwitch/PublisherSwitch'
import { connect } from 'react-redux'
import moment from 'moment'
import tr from 'moment/locale/tr'
import Icon from 'react-native-vector-icons/MaterialIcons'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
Icon.loadFont();
moment.locale('tr',tr)

class MainSearch extends Component {
  static navigationOptions = ({ navigation }) => {
      return {
          title: 'Arama',
          headerLeftContainerStyle: {paddingLeft: 5},
          headerRight: () => (
              <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8'
                onPress={() => navigation.goBack()}>
                <Icon
                name="close"
                color="#000"
                size={24}
                />
              </TouchableHighlight>
          ),
      } 
  };

  state = {
    publish_date: this.props.publish_date,
  }

  componentDidMount() {
    this.props.onFetchPublishers();
  }
  
  handleChange = publish_date => {
    const newPublishDate = moment(publish_date).format("YYYY-MM-DD");
    this.props.updatePublishDate(newPublishDate);
  }

  renderHeader = () => {
    return (
      <React.Fragment>
      <DatePicker
          style={{width: 1, height: 1}}
          date={new Date(this.props.publish_date)}
          mode="date"
          locale="tr_TR"
          placeholder="Tarih Seçiniz"
          format="YYYY-MM-DD"
          minDate="2016-05-01"
          maxDate={new Date()}
          confirmBtnText="Onayla"
          cancelBtnText="İptal"
          showIcon={false}
          hideText={true}
          ref={(ref)=>this.datePickerRef=ref}
          customStyles={{
            dateIcon: {
              display: 'none'
            },
            dateInput: {
              display: 'none'
            }
          }}
          onDateChange={this.handleChange}
        />
        <Text style={styles.inputLabel}>YAYIN TARİHİ</Text>
        <View style={styles.inputGroup}>
          <Text onPress={() => this.datePickerRef.onPressDate()} style={styles.date}>
          { moment(this.props.publish_date, 'YYYY-MM-DD').format("DD MMMM YYYY dddd") }
          </Text>
          <TouchableHighlight 
            hitSlop={{top: 20, bottom: 10, left: 20, right: 20}}
            underlayColor='#fff'
            onPress={() => this.datePickerRef.onPressDate()}>
            <MIcon 
              name="calendar-month"
              iconStyle={{marginRight: -5}}
              color="#000"
              size={24} />
          </TouchableHighlight>
        </View>
        <Text style={styles.inputLabel}>GAZETELER</Text>
        </React.Fragment>
    )
  }

  render() {
    return (
        <View style={styles.container}>
          <FlatList 
            ListHeaderComponent={this.renderHeader}
            data={this.props.publishers} 
            renderItem={({item}) => <PublisherSwitch toggle={() => this.props.onFilterPublisher(item.id)} publisher={item.name} is_visible={ this.props.disabledPublishers.includes(item.id) ? false : true} />} 
            keyExtractor={(item) => item.id.toString()} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  headerTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  headerCloseIcon: {
    position: 'absolute',
    right: 0
  },
  inputLabel: {
    color: '#8c8c8c',
    fontSize: 12,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingHorizontal: 15
  },
  filterContainer: {
    paddingTop: 20,
    paddingHorizontal: 15
  },
  inputGroup: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    paddingHorizontal: 15
  },
  publisherGroup: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  publisher: {
    fontSize: 18,
    flex: 1
  },
  date: {
    fontSize: 18,
    flex: 1
  }
})

const mapStateToProps = state => {
  return {
      publish_date: state.latestPublishDate.publish_date,
      publishers: state.publisher.publishers,
      disabledPublishers: state.publisher.disabledPublishers
  }
}

const mapDispatchToProps = dispatch => {
  return {
      updatePublishDate: (publish_date) => dispatch(actions.updatePublishDate(publish_date)),
      onFetchPublishers: () => dispatch(actions.fetchPublishers()),
      onFilterPublisher: (id) => dispatch(actions.filterPublisher(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainSearch)
