import React, {Component} from 'react'
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native'
import DatePicker from 'react-native-datepicker'
import * as actions from '../../store/actions/index'
import { connect } from 'react-redux'
import moment from 'moment'
import tr from 'moment/locale/tr'
import Icon from 'react-native-vector-icons/MaterialIcons'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
Icon.loadFont()
moment.locale('tr',tr)

class DateSearch extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Arama',
            headerLeftContainerStyle: {paddingLeft: 5},
            headerRight: () => (
              <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8'
                onPress={() => navigation.goBack()}>
                <Icon
                  name="close"
                  color="#000"
                  size={24}/>
              </TouchableHighlight>
            ),
        } 
    };

  state = {
    publish_date: this.props.publish_date,
  }
  
  handleChange = publish_date => {
    const newPublishDate = moment(publish_date).format("YYYY-MM-DD");
    this.props.updatePublishDate(newPublishDate);
  }

  render() {
    return (
        <View style={styles.container}>
          <View style={styles.filterContainer}>
          <DatePicker
              style={{width: 1, height: 1}}
              date={new Date(this.props.publish_date)}
              mode="date"
              locale="tr_TR"
              placeholder="Tarih Seçiniz"
              format="YYYY-MM-DD"
              minDate="2016-05-01"
              maxDate={new Date()}
              confirmBtnText="Onayla"
              cancelBtnText="İptal"
              showIcon={false}
              hideText={true}
              ref={(ref)=>this.datePickerRef=ref}
              customStyles={{
                dateIcon: {
                  display: 'none'
                },
                dateInput: {
                  display: 'none'
                }
              }}
              onDateChange={this.handleChange}
            />
            <Text style={styles.inputLabel}>YAYIN TARİHİ</Text>
            <View style={styles.inputGroup}>
              <Text onPress={() => this.datePickerRef.onPressDate()} style={styles.date}>{ moment(this.props.publish_date, 'YYYY-MM-DD').format("DD MMMM YYYY dddd") }</Text>
              <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 20}}
                underlayColor='#fff'
                onPress={() => this.datePickerRef.onPressDate()}>
                <MIcon 
                  name="calendar-month"
                  iconStyle={{marginRight: -5}}
                  color="#000"
                  size={24} />
              </TouchableHighlight>
            </View>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputLabel: {
    color: '#8c8c8c',
    fontSize: 12,
    fontWeight: 'bold',
  },
  filterContainer: {
    paddingTop: 20,
    paddingHorizontal: 15
  },
  inputGroup: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },
  date: {
    fontSize: 18,
    flex: 1
  }
})

const mapStateToProps = state => {
  return {
      publish_date: state.latestPublishDate.publish_date,
  }
}

const mapDispatchToProps = dispatch => {
  return {
      updatePublishDate: (publish_date) => dispatch(actions.updatePublishDate(publish_date)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DateSearch)
