import React, {Component} from 'react'
import { View, Text, StyleSheet, FlatList, TouchableHighlight } from 'react-native'
import * as actions from '../../store/actions/index'
import PublisherSwitch from '../../components/PublisherSwitch/PublisherSwitch'
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont();


class PublisherSearch extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Arama',
            headerLeftContainerStyle: {paddingLeft: 5},
            headerRight: () => (
              <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8'
                onPress={() => navigation.goBack()}>
                <Icon
                  name="close"
                  color="#000"
                  size={24}/>
              </TouchableHighlight>
            ),
        } 
    };

  componentDidMount() {
    this.props.onFetchPublishers();
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList 
          ListHeaderComponent={<Text style={styles.inputLabel}>GAZETELER</Text>}
          data={this.props.publishers} 
          renderItem={({item}) => <PublisherSwitch toggle={() => this.props.onAuthorFilterPublisher(item.id)} publisher={item.name} is_visible={ this.props.filteredPublishers.includes(item.id) || this.props.filteredPublishers.length == 0 ? true : false} />} 
          keyExtractor={(item) => item.id.toString()} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputLabel: {
    color: '#8c8c8c',
    fontSize: 12,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingHorizontal: 15
  },
})

const mapStateToProps = state => {
  return {
      publishers: state.publisher.publishers,
      filteredPublishers: state.publisher.filteredPublishers
  }
}

const mapDispatchToProps = dispatch => {
  return {
      onFetchPublishers: () => dispatch(actions.fetchPublishers()),
      onAuthorFilterPublisher: (id) => dispatch(actions.filterPublisher(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PublisherSearch)
