import React, { Component } from 'react'
import { WebView } from 'react-native-webview'
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';

class Article extends Component {
    static navigationOptions = () => {
        return {
            headerLeftContainerStyle: {paddingLeft: 5},
        }
    };
    
    componentDidMount() {
        this.props.onReadArticle(this.props.navigation.getParam('id'))
    }

    render() {
        const { navigation } = this.props;
        const url = JSON.stringify(navigation.getParam('article_url')).replace(/"/g, '')

        return (
            <WebView source={{uri: url}} />
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onReadArticle: (article_id) => dispatch(actions.readArticle(article_id)),
    }
}

export default connect(null, mapDispatchToProps)(Article)