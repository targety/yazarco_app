import React, { Component } from 'react'
import { ActivityIndicator, Alert, FlatList, View, StyleSheet, TouchableHighlight, Share  } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import Article from '../components/Article/Article'
import EmptyDataInfo from '../components/Common/EmptyDataInfo'
import * as actions from '../store/actions/index'
import Drawer from '../navigation/Drawer'
import _ from 'lodash'
Icon.loadFont();

class Explore extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Keşfet',
            headerLeft: <Drawer navigationProps={navigation} />,
            headerRight: () => (
                <TouchableHighlight 
                hitSlop={{top: 20, bottom: 10, left: 20, right: 10}}
                style={{paddingHorizontal: 15}} 
                underlayColor='#f8f8f8' 
                onPress={() => navigation.navigate('MainSearch')} >
                    <Icon
                    name="ios-search"
                    color="#000"
                    size={24}
                    />
                </TouchableHighlight>
            ),
        }
        
    };

    state = {
        articles: null
    }

    componentDidMount() {
        if(this.props.publish_date) {
            this.props.onFetchArticles(this.props.publish_date)

            if(this.props.token) {
                this.props.onFetchFavorites(this.props.token)
                this.props.onFetchBookmarks(this.props.token)
            }
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.publish_date !== prevProps.publish_date) {
            this.props.onFetchArticles(this.props.publish_date);
        }

        if(this.props.articles !== prevProps.articles) {
            const filteredArticle = this.props.articles.filter(article => !this.props.disabledPublishers.includes(article.publisher_id));
            this.setState({
                articles: _.shuffle(filteredArticle)
            });
        }

        if(this.props.disabledPublishers !== prevProps.disabledPublishers) {
            const filteredArticle = this.state.articles.filter(article => !this.props.disabledPublishers.includes(article.publisher_id));
            this.setState({
                articles: _.shuffle(filteredArticle)
            });
        }

        if((this.props.token !== prevProps.token) && this.props.token) {
            this.props.onFetchFavorites(this.props.token)
            this.props.onFetchBookmarks(this.props.token)
        }
    }

    redirectLogin() {
        Alert.alert(
            'Uyarı',
            'Kullanıcı girişi yapmalısınız',
            [
            {
                text: 'İptal',
                style: 'cancel',
                },
              {text: 'Giriş Yap', onPress: () => this.props.navigation.navigate('Login')},
            ],
            {cancelable: false},
          );
    }

    shareUrl = async (url) => {
        try {
            const result = await Share.share({
                message: url,
            });
        } catch (error) {
            alert(error.message);
        }
    } 

    render() {
        return (
            <View style={styles.container}>
                {
                this.props.loading ? 
                (<ActivityIndicator size="large" color="#000" />) : 
                (<FlatList 
                ListEmptyComponent={<EmptyDataInfo/>}
                data={this.state.articles} 
                renderItem={({item}) => 
                (
                <TouchableHighlight underlayColor='#f8f8f8' onPress={() => this.props.navigation.navigate('Article', {id: item.id, article_url: item.article_url})}>
                <Article 
                shareUrl={(url) => (this.shareUrl(url))}
                isFavorite={this.props.favorites.includes(item.id)} 
                addFavorite={(id, token) => this.props.isAuthenticated ? this.props.onAddFavorite(id, token) : this.redirectLogin()} 
                removeFavorite={(id, token) => this.props.isAuthenticated ? this.props.onRemoveFavorite(id, token): this.redirectLogin()}
                isBookmark={this.props.bookmarks.includes(item.id)} 
                addBookmark={(id, token) => this.props.isAuthenticated ? this.props.onAddBookmark(id, token) : this.redirectLogin()} 
                removeBookmark={(id, token) => this.props.isAuthenticated ? this.props.onRemoveBookmark(id, token): this.redirectLogin()}
                data={item} 
                token={this.props.token}/>
                </TouchableHighlight>
                )} 
                keyExtractor={(item) => item.id.toString()} />) 
                }
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
});

const mapStateToProps = state => {
    return {
        publish_date: state.latestPublishDate.publish_date,
        articles: state.article.articles,
        disabledPublishers: state.publisher.disabledPublishers,
        loading: state.article.loading,
        isAuthenticated: state.auth.token !== null,
        token: state.auth.token,
        favorites: state.auth.token ? state.article.favorites : [],
        bookmarks: state.auth.token ? state.article.bookmarks : [],
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchArticles: (publish_date) => dispatch(actions.fetchArticles(publish_date)),
        onAddFavorite: (id, token) => dispatch(actions.addFavorite(id, token)),
        onRemoveFavorite: (id, token) => dispatch(actions.removeFavorite(id, token)),
        onFetchFavorites: (token) => dispatch(actions.fetchFavorites(token)),
        onAddBookmark: (id, token) => dispatch(actions.addBookmark(id, token)),
        onRemoveBookmark: (id, token) => dispatch(actions.removeBookmark(id, token)),
        onFetchBookmarks: (token) => dispatch(actions.fetchBookmarks(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Explore)
