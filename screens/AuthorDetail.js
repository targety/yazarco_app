import React, { Component } from 'react'
import { Text, View, ActivityIndicator, Alert, StyleSheet, FlatList, TouchableHighlight, Share } from 'react-native'
import Article from '../components/Article/Article'
import { connect } from 'react-redux'
import * as actions from '../store/actions/index'
import AuthorInfoCard from '../components/Author/AuthorInfoCard'
import EmptyDataInfo from '../components/Common/EmptyDataInfo'

class AuthorDetail extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('name'),
            headerLeftContainerStyle: {paddingLeft: 5},
        }
    };

    state = {
        articles: [],
        selectedAuthor: this.props.navigation.getParam('id'),
        page: 1,
    }

    componentDidMount() {
        if(this.state.selectedAuthor) {
            this.props.onFetchAuthorArticles(this.state.page, this.state.selectedAuthor)

            if(this.props.token) {
                this.props.onFetchFavorites(this.props.token)
                this.props.onFetchBookmarks(this.props.token)
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.articles !== prevProps.articles) {
            this.setState({
                articles: this.state.articles.concat(this.props.articles)
            });
        }

        if(this.state.page !== prevState.page) {
            this.props.onFetchAuthorArticles(this.state.page, this.state.selectedAuthor)
        }
    }

    handleLoadMore = () => {
        if(this.props.hasNextPage) {
            this.setState({
                page: this.state.page + 1
            })
        }
    }

    renderHeader = () => {
        return <AuthorInfoCard 
        id={this.props.navigation.getParam('id')}
        token={this.props.token}
        isFollowing={this.props.followings.includes(this.props.navigation.getParam('id'))}
        followAuthor={(id, token) => this.props.isAuthenticated ? this.props.onFollowAuthor(id, token) : this.redirectLogin()}
        unfollowAuthor={(id, token) => this.props.isAuthenticated ? this.props.onUnfollowAuthor(id, token) : this.redirectLogin()}
        image={this.props.navigation.getParam('image')} />
    }

    renderFooter = () => {
        if(this.props.hasNextPage) {
            return <View style={{marginVertical: 10}}><ActivityIndicator size="large" color="#000" /></View>
        }
        return <View style={{marginVertical: 10, alignItems: 'center'}}><Text>Liste sonu</Text></View>
    }

    redirectLogin() {
        Alert.alert(
            'Uyarı',
            'Kullanıcı girişi yapmalısınız',
            [
            {
                text: 'İptal',
                style: 'cancel',
                },
              {text: 'Giriş Yap', onPress: () => this.props.navigation.navigate('Login')},
            ],
            {cancelable: false},
          );
    }

    shareUrl = async (url) => {
        try {
            const result = await Share.share({
                message: url,
            });
        } catch (error) {
            alert(error.message);
        }
    } 
    
    render() {
        return (
            <View style={styles.container}>
                {(
                <FlatList 
                ListEmptyComponent={<EmptyDataInfo loading={this.props.loading}/>}
                ListHeaderComponent={this.renderHeader}
                data={this.state.articles} 
                renderItem={({item}) => 
                (
                    <TouchableHighlight underlayColor='#f8f8f8' onPress={() => this.props.navigation.navigate('Article', {id: item.id, article_url: item.article_url})}>
                        <Article 
                        shareUrl={(url) => (this.shareUrl(url))}
                        isFavorite={this.props.favorites.includes(item.id)} 
                        addFavorite={(id, token) => this.props.isAuthenticated ? this.props.onAddFavorite(id, token) : this.redirectLogin()} 
                        removeFavorite={(id, token) => this.props.isAuthenticated ? this.props.onRemoveFavorite(id, token): this.redirectLogin()}
                        isBookmark={this.props.bookmarks.includes(item.id)} 
                        addBookmark={(id, token) => this.props.isAuthenticated ? this.props.onAddBookmark(id, token) : this.redirectLogin()} 
                        removeBookmark={(id, token) => this.props.isAuthenticated ? this.props.onRemoveBookmark(id, token): this.redirectLogin()}
                        data={item} 
                        token={this.props.token}/>
                    </TouchableHighlight>
                )} 
                keyExtractor={(item) => item.id.toString()}
                onEndReachedThreshold={0}
                onEndReached={this.handleLoadMore}
                ListFooterComponent={this.renderFooter} />
                ) }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center'
    },
})

const mapStateToProps = state => {
    return {
        loading: state.author.loading, 
        articles: state.author.articles,
        isAuthenticated: state.auth.token !== null,
        token: state.auth.token,
        favorites: state.auth.token ? state.article.favorites : [],
        bookmarks: state.auth.token ? state.article.bookmarks : [],
        hasNextPage: state.author.hasNextPage,
        followings: state.author.followings,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchAuthorArticles: (page, author_id) => dispatch(actions.fetchAuthorArticles(page, author_id)),
        onFetchFavorites: (token) => dispatch(actions.fetchFavorites(token)),
        onFetchBookmarks: (token) => dispatch(actions.fetchBookmarks(token)),
        onAddFavorite: (id, token) => dispatch(actions.addFavorite(id, token)),
        onRemoveFavorite: (id, token) => dispatch(actions.removeFavorite(id, token)),
        onAddBookmark: (id, token) => dispatch(actions.addBookmark(id, token)),
        onRemoveBookmark: (id, token) => dispatch(actions.removeBookmark(id, token)),
        onFollowAuthor: (id, token) => dispatch(actions.followAuthor(id, token)),
        onUnfollowAuthor: (id, token) => dispatch(actions.unfollowAuthor(id, token)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorDetail)
