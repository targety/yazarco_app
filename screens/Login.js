import React, { Component, Fragment } from 'react'
import { Text, View, TextInput, StyleSheet, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Drawer from '../navigation/Drawer'
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import * as yup from 'yup'
import { Formik } from 'formik'
Icon.loadFont();

class Login extends Component {
    static navigationOptions = ({ navigation }) => {
    return {
        title: 'Giriş',
        headerLeft: <Drawer navigationProps={navigation} />,
      };
    }

    render() {
        if (this.props.isAuthenticated) {
            this.props.navigation.navigate('Main')
        }

        const validationSchema = yup.object().shape({
            email: yup
              .string()
              .email('Geçersiz eposta formatı.')
              .required('Eposta alanı boş geçilemez.'),
            password: yup
              .string()
              .min(6, 'Parola alanı minumum 6 karakterli olmalıdır.')
              .required('Parola alanı boş geçilemez.'),
        });

        return (
            <KeyboardAvoidingView behavior="padding" enabled style={styles.container}>
                <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={values => this.props.onLogin(values.email, values.password)}
                validationSchema={validationSchema}
                >
                {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                <Fragment>
                <View style={styles.inputContainer}>
                    <Text style={styles.inputLabel}>Eposta</Text>
                    <TextInput
                    value={values.email}
                    onChangeText={handleChange('email')}
                    onBlur={() => setFieldTouched('email')} 
                    style={styles.input} 
                    type='email' 
                    placeholder={'Eposta adresinizi giriniz'} 
                    keyboardType='email-address' 
                    autoCapitalize='none'>
                    </TextInput>
                    {touched.email && errors.email &&
                    <Text style={styles.error}>{errors.email}</Text>
                    }
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.inputLabel}>Parola</Text>
                    <TextInput
                    value={values.password}
                    onChangeText={handleChange('password')}
                    onBlur={() => setFieldTouched('password')} 
                    style={styles.input} 
                    placeholder={'Parolanızı giriniz'} 
                    secureTextEntry={true}>
                    </TextInput>
                    {touched.password && errors.password &&
                    <Text style={styles.error}>{errors.password}</Text>
                    }
                </View>
                <View style={styles.inputContainer}>
                    <TouchableOpacity disabled={!isValid} onPress={handleSubmit} style={styles.button}>
                        <Text style={styles.buttonLabel}>Giriş</Text>
                    </TouchableOpacity>
                </View>
                </Fragment>
                )}
                </Formik>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    inputContainer: {
        alignItems: 'flex-start', 
        width: '80%', 
        marginBottom: 20
    },
    inputLabel: {
        color: '#4a5568', 
        fontWeight: 'bold', 
        fontSize: 16, 
        marginBottom: 5
    },
    input: {
        height: 40, 
        borderWidth: 1, 
        width: '100%', 
        borderColor: '#e2e8f0', 
        paddingHorizontal: 10, 
        fontSize: 16, 
        color: '#4a5568'   
    },
    button: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#4a5568',
        padding: 10
    },
    buttonLabel: {
        color: '#fff', 
        fontSize: 18, 
        fontWeight: 'bold'
    },
    error: {
        fontSize: 12,
        color: 'red',
        paddingTop: 5
    }
})

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (email, password) => dispatch(actions.login(email, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)